<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;


class ProfilesController extends Controller
{
    public function index(\App\User $user) {
        $follows = (auth()->user()) ? auth()->user()->following->contains($user->profile) : false;
        $followersCount = Cache::remember(
            "count.posts." . $user->id,
            now()->addSeconds(30),
            function() use ($user) {
                return $user->profile->followers->count();
            });

        $followingCount = $user->following->count();
        $postsCount = $user->posts()->count();

        return view("profiles.index", compact(
            "user",
            "follows",
            "followersCount",
            "followingCount",
            "postsCount"
        ));
    }

    public function edit(\App\User $user) {
        $this->authorize("update", $user->profile);
        return view("profiles.edit", compact("user"));
    }

    public function update(\App\User $user) {
        $data = request()->validate([
           "title" => "string|max:100",
            "description" => "string|max:300",
            "url" => "url",
            "image" => ""
        ]);

        if(request("image")) {
            $imgPath = request("image")->store("profile", "public");

            $image = Image::make(public_path("storage/".$imgPath))->fit(400, 400);
            $image->save();
            auth()->user()->profile->update(array_merge($data, ["image" => $imgPath]));
        } else {
            auth()->user()->profile->update(array_merge($data));
        }



        return redirect("/profile/{$user->id}");

    }
}
