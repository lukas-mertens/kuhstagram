<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function index() {
        $users = auth()->user()->following()->pluck("profiles.user_id");
        $posts = \App\Post::whereIn("user_id", $users)->with("user")->latest()->get();
        $userFinderUsers = User::inRandomOrder()->take(6)->get();

        return view("posts.index", compact("posts", "userFinderUsers"));
    }

    public function create() {
        return view("posts.create");

    }

    public function show(\App\Post $post) {
        return view("posts.show", [
            "post" => $post
        ]);
    }

    public function store() {
        $data = request()->validate([
           'caption' => 'required',
            'image' => ['required', 'image']
        ]);

        $imgPath = request("image")->store("uploads", "public");

        $image = Image::make(public_path("storage/".$imgPath))->fit(1200, 1200);
        $image->save();

        auth()->user()->posts()->create([
           "caption"  => $data["caption"],
            "image" => $imgPath
        ]);

        return redirect("/profile/".auth()->user()->id);
    }
}
