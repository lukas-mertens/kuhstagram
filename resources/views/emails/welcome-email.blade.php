@component('mail::message')
# Willkommen

Willkommen auf Kuhstagram!

@component('mail::button', ['url' => 'http://kuhstagram.kuhgle.top'])
Kühe sind toll!
@endcomponent

Danke für's registrieren!<br>
@endcomponent
