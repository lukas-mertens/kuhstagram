@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-3">
            <div class="col-8 offset-2">
                <div class="card">
                    <div class="card-header">Kuh-Fans finden</div>
                    <div class="card-body d-flex justify-content-center">
                        @foreach($userFinderUsers as $user)
                            <a href="/profile/{{$user->id}}" style="width:15%;">
                                <img src="{{ $user->profile->profileImage() }}" alt="" style="width:100%;" class="rounded-circle">
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
                <div class="col-4">
                    <div class="card card-bg-secondary m-1">
                        <div class="card-body">
                            <a href="/p/{{ $post->id }}"><img src="storage/{{ $post->image }}" alt="" class="w-100"></a>
                            <a class="text-black-50" href="/profile/{{ $post->user->id }}"><strong>{{$post->user->username}}</strong></a> <div>{{ $post->caption }}</div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
