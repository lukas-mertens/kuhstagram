@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1 p-5" style="background-color: white; box-shadow: rgba(0,0,0,0.2) 5px 5px 5px 5px">
                <div class="row">
                    <div class="col-8">
                        <a href="/profile/{{ $post->user->id }}">
                            <img src="/storage/{{ $post->image }}" alt="" class="w-100">
                        </a>
                    </div>
                    <div class="col-4" style="border-left: 1px solid gray">
                        <div class="d-flex align-items-center justify-content-start">
                            <a href="/profile/{{ $post->user->id }}" width="50px">
                                <img src="{{ $post->user->profile->profileImage() }}" alt="" class="rounded-circle" width="50px">
                            </a>
                            <h5 class="font-weight-bold ml-3" style="font-size: 120;">{{ $post->user->username }}</h5>
                        </div>
                        <p class="m-3"><span class="font-weight-bold pr-3">{{ $post->user->username }}</span>{{ $post->caption }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
