@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            <img src="{{ $user->profile->profileImage() }}" style="max-width: 50%; border-radius: 100%; margin-left: 25%; margin-top: 25%">
        </div>
        <div class="col-9">
            <div class="d-flex justify-content-between align-items-baseline">
                <div class="d-flex align-items-center pb-3">
                    <div class="pr-3"><h1>{{ $user->username }}</h1></div>
                    <follow-button user-id="{{ $user->id }}" follows="{{ $follows }}"></follow-button>
                </div>
                @can('update', $user->profile)
                <a href="/p/create">Neues Kuhbild veröffentlichen</a>
                @endcan
            </div>
            @can('update', $user->profile)
            <a href="/profile/{{ $user->id }}/edit">Profil kuhfreundlicher machen</a>
            @endcan
            <div class="d-flex">
                <div class="mr-5"><strong>{{ $postsCount }}</strong> Kuhbilder</div>
                <div class="mr-5"><strong>{{ $followersCount }}</strong> Verfolger</div>
                <div class="mr-5"><strong>{{ $followingCount }}</strong> Gejagte</div>
            </div>
            <div class="pt-4 font-weight-bold">{{ $user->profile->title }}</div>
            <div>{{ $user->profile->description }}</div>
            <div><a href="{{ $user->profile->url }}">{{ $user->profile->url }}</a></div>
        </div>
    </div>
        <div class="row mt-5">
            @foreach($user->posts as $post)
                <div class="col-4 pb-4">
                    <a href="/p/{{ $post->id }}">
                        <img src="/storage/{{ $post->image }}" alt="" class="w-100">
                    </a>
                </div>
            @endforeach
        </div>
</div>
@endsection
